import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/auth/login/login';
import { Storage } from '@ionic/storage';
import { SystemService } from '../services/system-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  constructor(platform: Platform, statusBar: StatusBar,
    storage: Storage,
    systemService: SystemService,
    splashScreen: SplashScreen) {
    // platform.ready().then(() => {
    //   // Okay, so the platform is ready and our plugins are available.
    //   // Here you can do any higher level native things you might need.
    //   statusBar.styleDefault();
    //   splashScreen.hide();
    // });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      systemService.initializeApplication().subscribe((res) => {
        storage.get('isTerminalRegistered').then((res) => {
          console.log(res);
          storage.get('isEmployeeLoggedIn').then((res) => {
            systemService.checkLastUpdatedDate().subscribe((isvalid) => {
              console.log('isEmployeeLoggedIn', res);
              console.log('isvalid', isvalid);
              if (res == true && isvalid == true) {
                this.rootPage = TabsPage;
              } else {
                this.rootPage = LoginPage;
              }
            })
          })
        })
      })
    });

  }
}

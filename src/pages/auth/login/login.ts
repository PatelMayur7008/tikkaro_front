import { retry } from 'rxjs/operator/retry';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SystemService } from '../../../services/system-service';
import { API } from '../../../services/api-service';
import * as moment from 'moment';
import { TabsPage } from '../../tabs/tabs';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public loginForm: FormGroup;
  public otpForm: FormGroup;
  public isOTPSend: boolean = false;
  public otpCheck: any = 0;
  public otpFail: any;
  public userdata: any;
  constructor(public navCtrl: NavController,
    private formBuilder: FormBuilder,
    public navParams: NavParams,
    public api: API,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    private systemService: SystemService) {
    this.createForm();
  }


  loginFormSubmit() {
    let payload = this.loginForm.value;
    payload.otp = this.getRandomInt();
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.api.loginFormSubmit(payload).subscribe(response => {
      this.loginForm.reset();
      this.isOTPSend = true;
      this.api.getloginDetails(payload).subscribe(response => {
        this.userdata = JSON.parse(response._body);
        this.otpCheck = this.userdata.data.int_otp;
        loading.dismiss();
      }, error => {
        console.log(error);
      })
    }, error => {
      console.log(error);
    })
  }

  otpFormSubmit() {
    let payload = this.otpForm.value;
    if (payload.otp == this.otpCheck) {
      let currentDate = moment().format();
      this.systemService.setUserdata(this.userdata, currentDate).subscribe((res) => {
        this.navCtrl.setRoot(TabsPage);
      });
    } else {
      this.otpFail = 'Your OTP is Wrong';
    }
  }

  getRandomInt() {
    let random = Math.floor(100000 + Math.random() * 900000);
    return random;
  }



  private createForm() {
    this.loginForm = this.formBuilder.group({
      pin_code: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(6)])],
      phone_number: ['', Validators.compose([Validators.required])]
    });

    this.otpForm = this.formBuilder.group({
      otp: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(6)])],
    });
  }

}

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { API } from '../../services/api-service';
import { API_BASE_URL, API_IMG_URL } from '../../services/core/constants';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { DetailsPage } from '../details/details';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-appoiments',
  templateUrl: 'appoiments.html'
})
export class AppoimentsPage {
  appoiments: any = [];
  appoiments1: any = [];
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private api: API,
  ) {
    this.initializeItems();
  }

  initializeItems() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.api.getAppoiments({ id: res.data.id }).subscribe((res) => {
        this.appoiments1 = JSON.parse(res._body);
        loading.dismiss();
        this.filterItem();
        console.log(this.appoiments);
      })
    })

  }

  getStoreDetails(store) {
    console.log(store);
    this.navCtrl.push(DetailsPage, { store: store })
  }

  filterItem() {
    this.appoiments = this.appoiments1;
  }

  searchName(ev) {
    this.filterItem();
    var val = ev.target.value;
    console.log(val)
    console.log(this.appoiments)
    if (val && val.trim() != '') {
      this.appoiments = this.appoiments.filter((item) => {
        return (item.var_title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  appoimentsClick() {

  }

}

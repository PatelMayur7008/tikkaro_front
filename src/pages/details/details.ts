import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { API } from '../../services/api-service';
import { API_BASE_URL, API_IMG_URL } from '../../services/core/constants';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AgmCoreModule } from '@agm/core';
import { BookPage } from '../book/book';

@Component({
  selector: 'page-details',
  templateUrl: 'details.html'
})
export class DetailsPage {
  store;
  serviceList: any = [];
  serviceList1: any = [];
  activeTab: string = 'services';
  API_IMG_URL = API_IMG_URL;
  lat: number = 28.7041;
  lng: number = 77.1025;
  markers: any[] = [
    { lat: 28.7041, lng: 77.1025 },
    { lat: 37.902, lng: 95.7129 },
    { lat: 20.593, lng: 78.962 },
    { lat: 19.076, lng: 72.877 },
    { lat: 30.3753, lng: 69.345 }];
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private navParams: NavParams,
    private api: API,
  ) {
    this.store = this.navParams.get('store');
    console.log('this.store', this.store);
    this.serviceList1 = this.store.store_has_services;
    this.filterItem();
  }

  filterItem() {
    this.serviceList = this.serviceList1;
  }

  searchService(ev) {
    this.filterItem();
    var val = ev.target.value;
    if (val && val.trim() != '') {
      this.serviceList = this.serviceList.filter((item) => {
        return (item.serivce_title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  book(service) {
    this.navCtrl.push(BookPage, { service: service })
  }

  // initializeItems() {
  //   let loading = this.loadingCtrl.create({
  //     content: 'Please wait...'
  //   });
  //   loading.present();
  //   this.api.getStore().subscribe((res) => {
  //     this.storeList1 = JSON.parse(res._body);
  //     loading.dismiss();
  //     this.storeList1.forEach(element => {
  //       element.var_logo = this.API_IMG_URL + 'public/upload/store_logo/' + element.var_logo;
  //     });
  //     console.log(this.storeList);
  //   })
  // }

  changeTab() {

  }



}

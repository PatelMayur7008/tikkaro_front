import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { API } from '../../services/api-service';
import { API_BASE_URL, API_IMG_URL } from '../../services/core/constants';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { DetailsPage } from '../details/details';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  storeList: any = [];
  storeList1: any = [];
  API_IMG_URL = API_IMG_URL;
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private api: API,
  ) {
    this.initializeItems();
  }

  initializeItems() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.api.getStore().subscribe((res) => {
      this.storeList1 = JSON.parse(res._body);
      loading.dismiss();
      this.storeList1.forEach(element => {
        element.var_logo = this.API_IMG_URL + 'public/upload/store_logo/' + element.var_logo;
      });
      this.filterItem();
      console.log(this.storeList);
    })
  }

  getStoreDetails(store) {
    console.log(store);
    this.navCtrl.push(DetailsPage, { store: store })
  }

  filterItem() {
    this.storeList = this.storeList1;
  }

  searchName(ev) {
    this.filterItem();
    var val = ev.target.value;
    console.log(val)
    console.log(this.storeList)
    if (val && val.trim() != '') {
      this.storeList = this.storeList.filter((item) => {
        return (item.var_title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}

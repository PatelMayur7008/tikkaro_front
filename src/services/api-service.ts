import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';

import { APP_VERSION, FORCE_REINIT, API_BASE_URL, API_END_POINTS } from './core/constants';

@Injectable()
export class API {
  private apiEndPoint: any = API_END_POINTS;
  private API_BASE_URL: any = API_BASE_URL;

  constructor(private storage: Storage,
    private http: Http,
  ) {
  };

  loginFormSubmit(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.login}`, payload);
  }

  getloginDetails(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.checkLogin}`, payload);
  }

  getStore(): Observable<any> {
    return this.http.get(`${API_BASE_URL}${this.apiEndPoint.getStore}`);
  }

  getStoreDetails(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getStoreDetails}`, payload);
  }

  getTimeSlotsByStoreId(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getTimeSlotsByStoreId}`, payload);
  }

  getAppoiments(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getAppoiments}`, payload);
  }



}

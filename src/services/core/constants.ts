export const API_BASE_URL: string = `http://b6adbad0.ngrok.io/tikkaro/api`;
export const API_IMG_URL: string = `http://b6adbad0.ngrok.io/tikkaro/`;
export const APP_VERSION: string = '0.0.1';
export const FORCE_REINIT: boolean = true;
// export const ENV = {
//   production: false,
//   serverUrl: API_BASE_URLRequest URL:http://2dcca45b.ngrok.io/tikkaro/apibiz/authentication

// };
export const API_END_POINTS: any = {
  login: '/authentication',
  checkLogin: '/checkAuthentication',
  getStore: '/getStoreList',
  getStoreDetails: '/getStoreListDetailById',
  getTimeSlotsByStoreId: '/getTimeSlotsByStoreId',
  getAppoiments: '/getAppoinmentbyUserId',

};

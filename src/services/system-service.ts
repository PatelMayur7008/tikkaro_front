import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';

import { APP_VERSION, FORCE_REINIT } from './core/constants';

@Injectable()
export class SystemService {
  // private syncServiceCount = 0;

  constructor(private storage: Storage,

  ) { };

  initializeApplication(): Observable<any> {
    return new Observable(ob => {
      this.getAppVersion().subscribe((appVersion) => {
        this.storage.get('isSystemInitialized').then((res) => {
          if (res == null) {
            this.storage.set('isSystemInitialized', true).then((value) => {
              let currentTime = moment().format('h:mm:ss');
              this.storage.set('isTerminalRegistered', false).then((value) => {
                this.reInitializeApplication().subscribe(() => ob.next());
              });
            });
          } else {
            if (appVersion != APP_VERSION) {
              this.setAppVersion().subscribe((newAppVersion) => {
                this.reInitializeApplication().subscribe(() => ob.next());
              });
            } else {
              ob.next();
            }
          }
        })
      })
    })
  }

  reInitializeApplication(): Observable<any> {
    return new Observable(ob => {
      if (FORCE_REINIT == true) {
        this.storage.set('isTerminalRegistered', false).then((value) => {
          this.employeeLogout().subscribe(() => {
            ob.next();
          });
        });
        //do code for reinitialize the application
      } else {
        ob.next();
      }
    });
  }

  getAppVersion(): Observable<any> {
    return new Observable(ob => {
      this.storage.get('appVersion').then((value) => {
        if (value != null) {
          ob.next(value);
        } else {
          this.setAppVersion().subscribe((value) => {
            ob.next(value);
          });
        }
      });
    })
  }

  setAppVersion(): Observable<any> {
    return new Observable(ob => {
      this.storage.set('appVersion', APP_VERSION).then((value) => {
        ob.next(value);
      });
    })
  }

  employeeLogin(data): Observable<any> {
    return new Observable(ob => {
      this.storage.set('isEmployeeLoggedIn', true).then((value) => {
        this.storage.set('loggedInEmployee', data).then((value) => {
          this.storage.set('isTerminalRegistered', false).then((value) => {
            ob.next(value);
          });
        });
      });
    });
  }




  employeeLogout(): Observable<any> {
    return new Observable(ob => {
      this.storage.set('isEmployeeLoggedIn', false).then((value) => {
        this.storage.set('loggedInEmployee', '').then((value) => {
          ob.next(value);
        });
      });
    });
  }

  getLoggedInEmployee(): Observable<any> {
    return new Observable(ob => {
      this.storage.get('loggedInEmployee').then((res) => {
        ob.next(res);
      })
    })
  }

  checkLastUpdatedDate(): Observable<any> {
    return new Observable(ob => {
      this.storage.get('lastUpdateDate').then((lastUpdateDate) => {
        let currentDate = moment().format();
        let currentDateplus15: any = moment().add(16, 'days');
        let lastUpdateDate1: any = moment(lastUpdateDate).add(1, 'days');
        let isvalid = moment(lastUpdateDate1).isBetween(currentDate, currentDateplus15);
        // console.log('ttttttttt', moment('2010-10-25').isBetween('2010-10-19', '2010-10-25'));
        // console.log('lastUpdateDate', moment('2010-10-25').isBetween('2010-10-19', '2010-10-25'));
        if (!isvalid) {
          this.getLoggedInEmployee();
        } else {
          let currentDate = moment().format();
          this.storage.set('lastUpdateDate', currentDate).then((res) => {
          })
        }
        ob.next(isvalid);
      })
    })
  }

  setUserdata(userdata: any, currentDate): Observable<any> {
    return new Observable(ob => {
      this.storage.set('isEmployeeLoggedIn', true).then((res) => {
        this.storage.set('loggedInEmployee', userdata).then((res) => {
          this.storage.set('lastUpdateDate', currentDate).then((res) => {
            ob.next();
          })
        });
      })
    })
  }


}
